module.exports = {
  env: {
    node: true,
  },
  root: true,
  parser: '@typescript-eslint/parser',
  overrides: [
    {
      files: [ '**/*.+(ts|tsx)', '**/*.spec.ts' ],
      parserOptions: {
        project: './tsconfig.json',
      },
      extends: [ 'airbnb-typescript/base' ],
    },
  ],
  plugins: [
    '@typescript-eslint',
    'eslint-plugin-import',
  ],
  extends: [
    'airbnb-base',
  ],
  rules: {
    'max-len': [ 2 ],
    'quote-props': [ 'error', 'as-needed' ],
    'comma-dangle': [ 'error', 'always-multiline' ],
    semi: [ 'error', 'always' ],
    quotes: [ 'error', 'single', { allowTemplateLiterals: true }],
    'key-spacing': [ 'error', { afterColon: true }],
    'array-bracket-spacing': [ 'error', 'always', { objectsInArrays: false, arraysInArrays: false }],
    'object-curly-spacing': [ 'error', 'always', { objectsInObjects: false, arraysInObjects: false }],
    'comma-spacing': [ 2, { before: false, after: true }],
    'no-restricted-syntax': 0,
    'no-await-in-loop': 0,
    'import/prefer-default-export': 0,
    'import/extensions': [ 'error', 'always' ],
    'no-plusplus': 0,
  },
};
