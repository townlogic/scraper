import { reqProperties } from '../src/scrapers/brisbane-devi.js';
import { AppType } from '../src/types.js';

describe('reqProperties', () => {
  it('should request properties', async () => {
    const data = await reqProperties(AppType.mcu, new Date('2022-02-01'), new Date('2022-02-04'));
    expect(data).toHaveProperty('features');
    expect(data?.features.length).toBeGreaterThan(0);
  });
});
