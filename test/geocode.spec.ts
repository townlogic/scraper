import { geocode, reverseGeocode } from '../src/lib/geocode.js';

describe('geocode', () => {
  it('should geocode by address', async () => {
    const data = await geocode('5 carrara st mt gravatt east');
    expect(data.items[0].title).toMatch(/5 carrara st/i);
  });
  it('should revgeocode by latitude, longitude', async () => {
    const data = await reverseGeocode(-27.58913055, 153.06151512);
    expect(data.items[0].title).toMatch(/21 Farne St/i);
  });
});
