import { downloadUploadPdf } from '../src/lib/fileUpload.js';
import { DocType, LGA } from '../src/types.js';

describe('fileUpload', () => {
  it('should download and upload a pdf file to Linode object storage', async () => {
    await downloadUploadPdf({
      lga: LGA.test,
      docType: DocType.test,
      url: 'https://developmenti.brisbane.qld.gov.au/DocumentSearch/downloadFile?fileId=227CD80C24C580D71DDE7158AFBDE1D0',
      appId: `_TEST_${Date.now()}_${Math.floor(Math.random() * 1000)}`,
    });
  });
});
