/* eslint-disable import/prefer-default-export */

export enum LGA {
  test = '_test',
  brisbane = 'brisbane',
  'gold-coast' = 'gold-coast',
}

export enum AppType {
  rol = 'rol',
  mcu = 'mcu',
}

export enum DocType {
  test = '_test',
  nad = 'nad',
  irr = 'irr',
  ir = 'ir',
}

export type SupportedDocType = DocType.nad | DocType.ir;

export enum DecisionType {
  approved = 'approved',
  refused = 'refused',
  appealed = 'appealed',
}

export enum ViewSystem {
  developmenti = 'developmenti',
  masterview = 'masterview',
  epathway = 'epathway',
}

export interface LGAScraperFunc {
  (start: Date, end: Date, docType: SupportedDocType): Promise<void>;
}
