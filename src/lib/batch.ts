import df from 'date-fns';

export const splitIntervalByMax = (
  start: Date,
  end: Date,
  maxDays: number,
): { start: Date, end: Date }[] => {
  const batch: { start: Date, end: Date }[] = [];
  let currentStart = start;
  while (df.isBefore(currentStart, end)) {
    const nextStart = df.add(currentStart, { days: maxDays });
    batch.push({
      start: currentStart,
      end: df.min([ nextStart, end ]),
    });
    currentStart = nextStart;
  }
  return batch;
};
