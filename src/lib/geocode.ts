import got from 'got';
import config from '../../config.js';

export const geocode = async (address: string): Promise<any> => {
  try {
    return await got({
      url: 'https://geocode.search.hereapi.com/v1/geocode',
      searchParams: {
        apiKey: config.hereApi.apiKey,
        q: address,
        in: 'countryCode:AUS',
        limit: 5,
      },
    }).json();
  } catch (err: any) {
    console.log(err?.response.body);
    return null;
  }
};

export const reverseGeocode = async (lat: number, lon: number): Promise<any> => {
  try {
    return await got({
      url: 'https://revgeocode.search.hereapi.com/v1/revgeocode',
      searchParams: {
        apiKey: config.hereApi.apiKey,
        at: `${lat},${lon}`,
        // in: 'countryCode:AUS',
        limit: 5,
      },
    }).json();
  } catch (err: any) {
    console.log(err?.response.body);
    return null;
  }
};
