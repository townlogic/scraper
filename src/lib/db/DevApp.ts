import mongoose from 'mongoose';

const DevAppSchema = new mongoose.Schema(
  {
    appId: { type: String, index: true, unique: true },
    appKey: { type: String, index: true },
    appDesc: { type: String },
    appTypes: { type: [ String ] },
    decisionType: { type: String },
    dnDate: { type: Date },
    dnUrl: { type: String },
    lastCheckedAt: { type: Date },
    lga: { type: String, index: true },
    locations: [{
      city: { type: String },
      district: { type: String },
      houseNumber: { type: String },
      key: { type: String },
      label: { type: String },
      postalCode: { type: String },
      state: { type: String },
      stateCode: { type: String },
      street: { type: String },
      position: {
        lat: Number,
        lng: Number,
      },
    }],
    nadDate: { type: Date, index: true },
    nadPageUrl: { type: String, index: true },
    nadUrl: { type: String },
    viewSystem: { type: String },
    nadStoredLocal: { type: Boolean, index: true, default: false },
    irrStoredLocal: { type: Boolean, index: true, default: false },
    irStoredLocal: { type: Boolean, index: true, default: false },
  },
  { timestamps: true },
);

const DevApp = mongoose.model('DevApp', DevAppSchema);

export default DevApp;
