import mongoose from 'mongoose';
import config from '../../../config.js';

export const initDB = async () => {
  const mong = await mongoose.connect(config.mongoDbConn);
  console.log('MongoDB connected.');
  return mong.connection;
};
