import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import got from 'got';
import config from '../../config.js';
import { DocType, LGA } from '../types.js';

interface DownloadUploadParams {
  url: string,
  lga: LGA,
  docType: DocType,
  appId: string,
}

enum FileExtension {
  pdf = 'pdf',
}

const client = new S3Client({
  credentials: {
    accessKeyId: config.objectStorage.accessKeyId,
    secretAccessKey: config.objectStorage.secretAccessKey,
  },
  endpoint: config.objectStorage.endpoint,
  region: config.objectStorage.region,
});

export const getPdfUrl = (lga: LGA, docType: DocType, appId: string): string => `https://${config.objectStorage.pdfBucket}.${config.objectStorage.region}.linodeobjects.com/${lga}/${docType}-${appId}.${FileExtension.pdf}`;

export const uploadPdf = async (
  lga: LGA,
  docType: DocType,
  appId: string,
  data: Buffer,
) => {
  const bucket = config.objectStorage.pdfBucket;
  const key = `${lga}/${docType}-${appId}.${FileExtension.pdf}`;
  await client.send(new PutObjectCommand({
    Key: key,
    Bucket: bucket,
    Body: data,
    ACL: 'public-read',
    ContentType: 'application/pdf',
  }));
};

export const downloadUploadPdf = async ({
  url, lga, docType, appId,
}: DownloadUploadParams): Promise<void> => {
  const resp = await got(url);
  const fileData = Buffer.from(resp.rawBody);
  await uploadPdf(lga, docType, appId, fileData);
};
