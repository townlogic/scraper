import dateFns from 'date-fns';
import { Client } from '@elastic/elasticsearch';
import got from 'got';
import pLimit from 'p-limit';
import config from '../config.js';
import DevApp from './lib/db/DevApp.js';
import { getPdfUrl } from './lib/fileUpload.js';
import { initDB } from './lib/db/index.js';

const indices = [
  'nad',
  'irr',
  'ir',
];

const client = new Client({
  node: config.elasticSearch.url,
  maxRetries: 5,
  requestTimeout: 60000,
  sniffOnStart: true,
  auth: {
    username: config.elasticSearch.username,
    password: config.elasticSearch.password,
  },
});

const MAX_CONCURRENT_REQUESTS = 50;
const limit = pLimit(MAX_CONCURRENT_REQUESTS);

const countLogger = (label: string) => {
  let count = 0;
  return () => (++count % 5 || console.log(`${label}: ${count}`));
};

const runIndexer = async (updatedAtDaysBefore: number) => {
  const logIndexed = countLogger('Indexed');
  const logUpdated = countLogger('Updated');
  try {
    console.log(`Running indexer with updatedAtDaysBefore: ${updatedAtDaysBefore}`);

    // check for and create index if not exists
    await Promise.allSettled(indices.map((indexName) => (async () => {
      const exists = await client.indices.exists({ index: indexName });
      if (exists.body) {
        console.log(`Index exists: ${indexName}`);
        return;
      }
      console.log(`Creating index: ${indexName}`);
      await client.indices.create({
        index: indexName,
        body: {
          mappings: { dynamic: true },
        },
      });
    })()));

    // find devapps that were recently updated
    const devApps = await DevApp.find({
      updatedAt: { $gte: dateFns.sub(new Date(), { days: updatedAtDaysBefore }) },
      $or: [
        { nadStoredLocal: true },
        { irrStoredLocal: true },
        { irStoredLocal: true },
      ],
    });

    console.log('DevApps found:', devApps.length);

    await Promise.allSettled(devApps.map((devApp) => limit(async () => {
      // get pdf data
      const docTypes = [
        devApp.nadStoredLocal && 'nad',
        devApp.irrStoredLocal && 'irr',
        devApp.irStoredLocal && 'ir',
      ].filter(Boolean);

      await Promise.allSettled(docTypes.map((docType) => (async () => {
        const exists = (await client.exists({ id: devApp.appId, index: docType })).body;

        const upsertData = {
          id: devApp.appId,
          index: docType,
          body: {
            locations: devApp.locations.map((loc: any) => ({
              city: loc.city,
              street: loc.street,
              district: loc.district,
              postalCode: loc.postalCode,
              houseNumber: loc.houseNumber,
              stateCode: loc.stateCode,
              label: loc.label,
              state: loc.state,
              position: {
                lat: loc.position.lat,
                lon: loc.position.lng,
              },
            })),
            app_id: devApp.appId,
            decision_date: devApp.dnDate ? devApp.dnDate.toISOString() : null,
            app_type: devApp.appTypes,
            decision_type: devApp.decisionType,
            lga: devApp.lga,
            nad_stored_local: devApp.nadStoredLocal,
            irr_stored_local: devApp.irrStoredLocal,
            ir_stored_local: devApp.irStoredLocal,
            view_system: devApp.viewSystem,
          },
        };
        if (exists) { // updates only the fields (not the pdf)
          await client.update({
            ...upsertData,
            body: {
              doc: upsertData.body,
            },
          });
          logUpdated();
        } else { // index
          const pdfBuffer = await got(getPdfUrl(devApp.lga, docType, devApp.appId)).buffer();
          const pdfData = pdfBuffer.toString('base64');

          await client.index({
            ...upsertData,
            pipeline: 'nad_attachment',
            body: {
              ...upsertData.body,
              data: pdfData,
            },
          }).catch((err) => console.error(err.meta.body));
          logIndexed();
        }
      })()));
    })));
  } catch (err) {
    console.error('Error with indexer');
    console.error(err);
  }
};

(async () => {
  console.log('Indexer started.');
  console.info('Connecting DB...');
  const dbConnection = await initDB();

  const updatedAtDaysBefore = parseInt(process.argv[2] || '2', 10);
  await runIndexer(updatedAtDaysBefore);

  console.info('Indexing complete.');
  console.info('Closing DB connection...');
  await dbConnection.close();
  console.info('Exiting...');
  process.exit(0);
})();
