import got from 'got';
import pLimit from 'p-limit';
import DevApp from '../lib/db/DevApp.js';
import { splitIntervalByMax } from '../lib/batch.js';
import { downloadUploadPdf } from '../lib/fileUpload.js';
import {
  AppType,
  DecisionType, DocType, LGA, LGAScraperFunc, SupportedDocType, ViewSystem,
} from '../types.js';
import { geocode } from '../lib/geocode.js';

const MAX_BATCH_SIZE_DAYS = 5;
const MAX_CONCURRENT_REQUESTS = 30;
const limit = pLimit(MAX_CONCURRENT_REQUESTS);

const reqTypeMap = {
  rol: 'Reconfigure a Lot',
  mcu: 'Material Change of Use',
};

const decTypeMap: { [k: string]: DecisionType } = {
  Approved: DecisionType.approved,
};

interface AppDoc {
  applicationId: string
  category: string
  documentId: string
  fileDate: string
  fileName: string
  fileSize: number
  fileextension: string
  id: number
  newfileindicator: null
}

interface BrisbaneDA {
  appeal_result: string
  application_number: string
  application_type: string
  description: string
  assessment_level: string
  category: string
  category_desc: string
  date_created: string
  date_determined: string
  date_received: string
  decision_desc: string
  from_layer: string
  group_code: string
  group_desc: string
  kindofrecord: string
  land_no: string
  land_parcel_relationship: string
  pdonline_id: number
  progress: string
  project_officer: string
  submissionindicator: string
  uselevel1: string
  uselevel2: string
  app_type: AppType
  lat: number
  lon: number
}

const docTypeToStoredField = {
  [DocType.nad]: 'nadStoredLocal',
  [DocType.ir]: 'irStoredLocal',
};

const docCategories = {
  [DocType.nad]: 'Notice - Decision',
  [DocType.ir]: 'Information Request',
};

const findDocByType = (docType: SupportedDocType, docs: AppDoc[]): AppDoc | undefined => {
  const category = docCategories[docType];
  const doc = docs.find((d) => d.category === category);
  return doc;
};

const getPdfUrl = (documentId: string) => `https://developmenti.brisbane.qld.gov.au/DocumentSearch/downloadFile?fileId=${documentId}`;

export const reqProperties = async (appType: AppType, start: Date, end: Date, decided = false) => {
  const data = await limit(() => got({
    url: 'https://developmenti.brisbane.qld.gov.au/Geo/GetApplicationFilterResults',
    method: 'POST',
    json: {
      Progress: decided ? 'Decided' : 'all',
      StartDateUnixEpochNumber: start.getTime(),
      EndDateUnixEpochNumber: end.getTime(),
      DateRangeField: decided ? 'decided' : 'submitted',
      DateRangeDescriptor: null,
      LotPlan: null,
      LandNumber: null,
      PropNumber: null,
      DANumber: null,
      BANumber: null,
      PlumbNumber: null,
      IncludeDA: true,
      IncludeBA: false,
      IncludePlumb: false,
      LocalityId: null,
      DivisionId: null,
      ApplicationTypeId: reqTypeMap[appType],
      SubCategoryUseId: null,
      AssessmentLevels: [],
      ShowCode: true,
      ShowImpact: true,
      ShowIAGA: true,
      ShowIAGI: true,
      ShowNotifiableCode: true,
      ShowReferralResponse: true,
      ShowRequest: true,
      PagingStartIndex: 0,
      MaxRecords: 200,
      Boundary: null,
      ViewPort: {
        BoundaryType: 'POLYGON',
        GeometryPropertyName: 'geom_point',
        Boundary: [[{
          Lat: -27.73646095657793,
          Lng: 152.7474860025406,
        },
        {
          Lat: -27.73646095657793,
          Lng: 153.30271399745942,
        },
        {
          Lat: -27.202291767670335,
          Lng: 153.30271399745942,
        },
        {
          Lat: -27.202291767670335,
          Lng: 152.7474860025406,
        },
        {
          Lat: -27.73646095657793,
          Lng: 152.7474860025406,
        }]],
      },
      IncludeAroundMe: false,
      SortField: 'submitted',
      SortAscending: false,
      BBox: null,
      PixelWidth: 800,
      PixelHeight: 800,
    },
  }).json<any>());
  return data;
};

const reqDocuments = async (appId: string) => {
  const data = await limit(() => got({
    url: 'https://developmenti.brisbane.qld.gov.au/DocumentSearch/GetResult',
    method: 'POST',
    searchParams: {
      applicationId: appId,
      isSubCategory: false,
    },
  }).json<any>());
  return data?.data;
};

const runBatch = async (batchId: number, start: Date, end: Date, docType: SupportedDocType) => {
  console.info(`Batch ${batchId}: running`);
  const currentDate = new Date();
  const [ mcus, rols ] = await Promise.all(
    [
      AppType.mcu,
      AppType.rol,
    ].map((type) => reqProperties(type, start, end, docType === DocType.nad)),
  );

  const DAs = [
    ...mcus.features.map((f: any) => ({
      ...f.properties,
      app_type: AppType.mcu,
      lat: f.geometry.coordinates[1],
      lon: f.geometry.coordinates[0],
    })),
    ...rols.features.map((f: any) => ({
      ...f.properties,
      app_type: AppType.rol,
      lat: f.geometry.coordinates[1],
      lon: f.geometry.coordinates[0],
    })),
  ];

  const dasToFetch: BrisbaneDA[] = [];
  for (const da of DAs) {
    const exists = await DevApp.exists({ appId: da.application_number });
    if (!exists) {
      dasToFetch.push(da);
    }
  }

  await Promise.allSettled(dasToFetch.map((da) => (async () => {
    const daDocsParams = {
      lga: LGA.brisbane,
      appId: da.application_number,
    };
    const docs: AppDoc[] = await reqDocuments(da.application_number);
    const pdfDocs = docs.filter((doc) => doc.fileextension === 'pdf');

    const doc = findDocByType(docType, pdfDocs);
    if (doc) {
      await downloadUploadPdf({
        ...daDocsParams,
        docType,
        url: getPdfUrl(doc.documentId),
      });
      console.info(`${docType.toUpperCase()} uploaded!`, da.application_number);
    } else {
      console.info(`${docType.toUpperCase()} doc not found`, da.application_number);
      return;
    }

    const addressString = da.description.match(/([^-]+)\s-\s/i)?.[1];
    if (!addressString) {
      console.error(`Address not found in description ${da.description}`);
      return;
    }
    const geoData = await geocode(addressString);
    const geoItem = geoData?.items?.[0];
    const { address, position } = geoItem;

    await DevApp.findOneAndUpdate({ lga: LGA.brisbane, appId: da.application_number }, {
      lga: LGA.brisbane,
      appId: da.application_number,
      appTypes: [ da.app_type ],
      appDesc: da.description,
      decisionType: decTypeMap[da.decision_desc] || da.decision_desc.toLowerCase(),
      dnDate: da.date_determined ? new Date(da.date_determined) : null,
      lastCheckedAt: currentDate,
      nadDate: da.date_determined ? new Date(da.date_determined) : null,
      viewSystem: ViewSystem.developmenti,
      [docTypeToStoredField[docType]]: Boolean(doc),
      locations: [{
        city: address.city,
        district: address.district,
        houseNumber: address.houseNumber,
        label: address.label,
        postalCode: address.postalCode,
        state: address.state,
        stateCode: address.stateCode,
        street: address.street,
        position: {
          lat: position.lat,
          lng: position.lng,
        },
      }],
    }, { new: true, upsert: true });
  })()));
  console.debug(`Batch ${batchId} complete`);
};

const run: LGAScraperFunc = async (start, end, docType) => {
  const batches = splitIntervalByMax(start, end, MAX_BATCH_SIZE_DAYS);
  console.log(`Split into ${batches.length} batches`);
  await Promise.allSettled(batches.map(
    (batch, i) => runBatch(i + 1, batch.start, batch.end, docType),
  ));
};

export default run;
