import dateFns from 'date-fns';
import { initDB } from './lib/db/index.js';
import brisbane from './scrapers/brisbane-devi.js';
import { DocType, LGAScraperFunc, SupportedDocType } from './types.js';

const scrapers: { [k:string]: LGAScraperFunc } = {
  brisbane,
};

const scrapeDocTypes: SupportedDocType[] = [
  // DocType.nad,
  DocType.ir,
];

(async () => {
  console.info('Scraper started');
  console.info('Connecting DB...');
  const dbConnection = await initDB();

  const lga = process.argv[2];
  const scraper = scrapers[lga];
  if (!scraper) {
    console.error(`Scraper not found for "${lga}"`);
    process.exit(1);
  }
  const dateArg1 = process.argv[3];
  const dateArg2 = process.argv[4];

  if (!dateArg1) {
    console.error('Missing date argument');
    process.exit(1);
  }

  let lookbackDays;
  let startDate;
  let endDate;
  if (!dateArg2) {
    lookbackDays = parseInt(dateArg1, 10);
    if (!lookbackDays || lookbackDays < 1) {
      console.error(`Start days ago is invalid: ${lookbackDays}`);
      process.exit(1);
    }
    endDate = new Date();
    startDate = dateFns.sub(endDate, { days: lookbackDays });
  } else {
    startDate = new Date(dateArg1);
    endDate = new Date(dateArg2);
  }

  for (const docType of scrapeDocTypes) {
    console.info(`Running ${lga} scraper for ${docType.toUpperCase()} from ${dateFns.format(startDate, 'yyyy-MM-dd')} to ${dateFns.format(endDate, 'yyyy-MM-dd')}...`);
    await scraper(startDate, endDate, docType);
  }
  console.info('Scrape complete.');
  console.info('Closing DB connection...');
  await dbConnection.close();
  console.info('Exiting...');
  process.exit(0);
})();
