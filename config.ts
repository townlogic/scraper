export default {
  mongoDbConn: '',
  objectStorage: {
    pdfBucket: 'pdf',
    endpoint: '',
    region: '',
    accessKeyId: '',
    secretAccessKey: '',
  },
  hereApi: {
    appId: '',
    apiKey: '',
  },
  elasticSearch: {
    url: '',
    username: '',
    password: '',
  },
};
