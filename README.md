# TownLogic Scraper

## Requirements
- Node.js v14
- yarn

## Setup

```sh
yarn
```

## Lint

```sh
yarn lint
```

## Test

```sh
yarn test
```

## Scrape

Scrape by `lga` and either:
- `lookback-days`, i.e. start date is lookback-days before today, end date is today
- `start-date` and `end-date`

```sh
yarn scrape <lga> <lookback-days>

# OR

yarn scrape <lga> <start-date> <end-date>
```
e.g.
```sh
yarn scrape brisbane 5

# OR

yarn scrape brisbane 2022-01-15 2022-01-20
```

### ESM setup
https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c